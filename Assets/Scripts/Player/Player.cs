﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Light m_flashlight;

    [SerializeField]
    private AudioSource m_walkSound;

    [SerializeField]
    private AudioSource m_runSound;

    [SerializeField]
    private AudioSource m_deadSound;

    [SerializeField]
    private AudioSource m_seeEnemySound;

    [SerializeField]
    private Camera m_camera;

    private CharacterController m_controller;
    private float m_speed = 3.0f;
    private float m_gravity = 1.0f;
    [SerializeField]
    private bool m_isWalking = false;
    [SerializeField]
    private bool m_isRunning = false;
    private float m_stamina = 100f;
    private float m_staminaRechargeRate = 0.5f;
    private float m_staminaCostRate = 1f;
    private bool m_isLightOn = true;
    private bool m_isAlive = true;
    private int m_inventoryCount = 0;
    private GameManager m_gameManager;
    private UIManager m_uiManager;

    // Start is called before the first frame update
    void Start()
    {
        m_controller = GetComponent<CharacterController>();
        m_gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        m_uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        //m_staminaBar = GameObject.Find("StaminaBar_Image").GetComponent<Image>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_isAlive)
        {
            CalculateMovement();
            HandleFlashLight();
            HandleFootstepsSounds();

            RaycastHit hitInfo;

            if (Physics.Raycast(m_flashlight.transform.position, m_flashlight.transform.TransformDirection(Vector3.forward), out hitInfo, 1000f))
            {
                Debug.Log(hitInfo.transform.name);
                // If the Player is looking at the Enemy
                if (hitInfo.transform.tag == "Slenderman")
                {
                    // Let the Enemy know he's been seen by the Player
                    Enemy enemy = hitInfo.transform.GetComponent<Enemy>();
                    enemy.SetSeenByPlayer();

                    // Play some scary horror sound for lolz
                    if (!m_seeEnemySound.isPlaying)
                    {
                        m_seeEnemySound.Play(0);
                    }                        

                    // Turn Static effect on
                    m_uiManager.SetStaticImageVisible(true, true);
                }

                if (hitInfo.transform.tag != "Slenderman")
                {
                    // If the Player isn't looking at the enemy then we're good
                    // In case the player is still in proximity of the Enemy, static effect will still take place (Enemy.cs)
                    m_uiManager.SetStaticImageVisible(false, true);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    void CalculateMovement()
    {
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            if (Input.GetKey("left shift") || Input.GetKey("right shift"))
            {
                // Running
                m_isWalking = false;
                m_isRunning = true;
                m_speed = 6.0f;
                m_stamina = Mathf.Max(0f, m_stamina - m_staminaCostRate);
            }
            else
            {
                // Walking
                m_isWalking = true;
                m_isRunning = false;
                m_speed = 3.0f;
            }
        }
        else
        {
            // Stopped
            m_isWalking = false;
            m_isRunning = false;
        }

        m_stamina = Mathf.Min(100f, m_stamina + m_staminaRechargeRate);
        Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Vector3 velocity = direction * m_speed * Time.deltaTime;
        velocity.y -= m_gravity;
        velocity = transform.TransformDirection(velocity);
        m_controller.Move(velocity);
    }

    void HandleFootstepsSounds()
    {
        if (m_isWalking)
        {
            if (m_runSound.isPlaying)
            {
                m_runSound.Stop();
            }

            if (!m_walkSound.isPlaying)
            {
                m_walkSound.Play();
            }
        }
        else if (m_isRunning)
        {
            if (m_walkSound.isPlaying)
            {
                m_walkSound.Stop();
            }

            if (!m_runSound.isPlaying)
            {
                m_runSound.Play();
            }
        }
        else
        {
            m_walkSound.Stop();
            m_runSound.Stop();
        }
    }

    void HandleFlashLight()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            m_isLightOn = !m_isLightOn;
            m_flashlight.enabled = m_isLightOn;
        }

        if (m_isLightOn)
        {
            m_flashlight.intensity -= 0.0003f;
        }
    }

    public int GetInventoryCount()
    {
        return m_inventoryCount;
    }

    public void IncreaseInventory()
    {
        m_inventoryCount++;
    }
    
    public bool IsAlive()
    {
        return m_isAlive;
    }

    public void Die()
    {
        m_isAlive = false;
        m_deadSound.Play(0);
    }
}
