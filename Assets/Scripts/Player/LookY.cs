﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookY : MonoBehaviour
{
    [SerializeField]
    private float _sensitivity = 2.5f;

    private Player m_player;

    private void Start()
    {
        m_player = GameObject.Find("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_player.IsAlive())
        {
            float mouseY = Input.GetAxis("Mouse Y");
            Vector3 newRotation = transform.localEulerAngles;
            newRotation.x += mouseY * _sensitivity * -1;

            if (newRotation.x < 300 && newRotation.x > 60)
            {
                if (newRotation.x < 100)
                    newRotation.x = 60;
                else
                    newRotation.x = 300;
            }

            transform.localEulerAngles = newRotation;
        }        
    }
}
