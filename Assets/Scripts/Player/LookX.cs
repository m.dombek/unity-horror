﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookX : MonoBehaviour
{
    [SerializeField]
    private float _sensitivity = 2.5f;

    private Player m_player;

    private void Start()
    {
        m_player = GameObject.Find("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_player.IsAlive())
        {
            float mouseX = Input.GetAxis("Mouse X");
            Vector3 newRotation = transform.localEulerAngles;
            newRotation.y += mouseX * _sensitivity;
            transform.localEulerAngles = newRotation;
        }
    }
}
