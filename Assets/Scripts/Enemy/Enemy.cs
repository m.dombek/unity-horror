﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public Player m_player;
    private AudioSource m_staticSound;
    private Animator m_animator;
    private CharacterController m_controller;
    private GameManager m_gameManager;
    private UIManager m_uiManager;
    private Spawner m_spawner;
    private float m_minDistanceToHurtPlayer = 12f;
    private bool m_seenByPlayer = false;

    // Start is called before the first frame update
    void Start()
    {
        m_animator = GetComponent<Animator>();
        m_staticSound = GetComponent<AudioSource>();
        m_controller = GetComponent<CharacterController>();
        m_gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        m_player = GameObject.Find("Player").GetComponent<Player>();
        m_uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        m_spawner = transform.parent.gameObject.GetComponent<Spawner>();

        m_animator.CrossFade("Idle", 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        m_controller.Move(Vector3.down);

        if (m_player != null)
        {
            // Face Player
            transform.LookAt(m_player.transform);

            if (m_seenByPlayer)
            {
                // Make static noise
                m_staticSound.volume = 10 / Vector3.Distance(transform.position, m_player.transform.position);

                float distanceFromPlayer = Vector3.Distance(transform.position, m_player.transform.position);

                m_uiManager.SetStaticImageVisible(distanceFromPlayer < m_minDistanceToHurtPlayer, false);

                //m_minDistanceToHurtPlayer -= Time.deltaTime * 0.5f;

                if (distanceFromPlayer > 30f || m_minDistanceToHurtPlayer < 5f)
                {
                    Despawn();
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            m_animator.CrossFade("Scream", 0.1f);
            m_gameManager.GameOver();
        }
    }

    public void SetSeenByPlayer()
    {
        m_seenByPlayer = true;
    }

    public void Despawn()
    {
        m_uiManager.SetStaticImageVisible(false, m_seenByPlayer);
        Destroy(gameObject);
        m_spawner.HandleDespawn();
    }
}
