﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_enemyPrefab;

    [SerializeField]
    private GameObject[] m_spawnPoints;

    [SerializeField]
    private int m_spawnProbability;

    private Enemy m_spawnedEnemy;
    private bool m_isSpawned = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!m_isSpawned && other.tag == "Player")
        {
            Player player = GameObject.Find("Player").GetComponent<Player>();
            int spawnRandomValue = Random.Range(0, 100) + (player.GetInventoryCount() * 10);

            // If the Spawner should spawn according to the generated value
            if (spawnRandomValue <= m_spawnProbability)
            {
                GameObject otherSpawn = GameObject.FindGameObjectWithTag("Slenderman");

                // If another Slenderman spawn already exists elsewhere, destroy it
                if (otherSpawn)
                {
                    Destroy(otherSpawn.gameObject);
                }

                GameObject spawnPoint = m_spawnPoints[player.GetInventoryCount()];
                m_spawnedEnemy = Instantiate(m_enemyPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation).GetComponent<Enemy>();
                m_spawnedEnemy.transform.SetParent(transform);
                m_isSpawned = true;
            }
        }
    }

    public void HandleDespawn()
    {
        m_isSpawned = false;
    }
}
