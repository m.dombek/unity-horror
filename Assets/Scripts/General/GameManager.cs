﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private UIManager m_uiManager;
    private Player m_player;
    private int m_encounters = 0;
    private bool m_isGameOver = false;

    private void Start()
    {
        m_uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        m_player = GameObject.Find("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.R) && m_isGameOver == true)
        {
            // File -> Build Settings -> Add Open Scenes
            // This will add a scene id to the project
            SceneManager.LoadScene(1);
        }
    }

    public int GetEncounters()
    {
        return m_encounters;
    }

    public void IncreaseEncounters()
    {
        m_encounters++;
        Debug.Log("Encounters: " + m_encounters);
    }

    public void GameOver()
    {
        m_player.Die();
        m_uiManager.SetDeathText(true);
        m_isGameOver = true;
    }
}
