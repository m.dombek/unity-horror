﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItemManager : MonoBehaviour
{
    [SerializeField]
    private GameObject m_key;

    [SerializeField]
    private GameObject m_syringe;

    [SerializeField]
    private GameObject m_mirror;

    private GameObject[] m_spawnPoints;
    private GameObject m_keySpawnPoint;
    private GameObject m_syringeSpawnPoint;
    private GameObject m_mirrorSpawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        // Init random item locations
        int keySpawnIndex, syringeSpawnIndex, mirrorSpawnIndex;

        m_spawnPoints = GameObject.FindGameObjectsWithTag("KeyItem_SpawnPoint");

        keySpawnIndex = Random.Range(0, m_spawnPoints.Length);
        syringeSpawnIndex = Random.Range(0, m_spawnPoints.Length);
        mirrorSpawnIndex = Random.Range(0, m_spawnPoints.Length);

        // Keep generating until a distinct number was found
        while (syringeSpawnIndex == keySpawnIndex)
        {
            syringeSpawnIndex = Random.Range(0, m_spawnPoints.Length);
        }
        
        while (mirrorSpawnIndex == keySpawnIndex || mirrorSpawnIndex == syringeSpawnIndex)
        {
            mirrorSpawnIndex = Random.Range(0, m_spawnPoints.Length);
        }

        m_keySpawnPoint = m_spawnPoints[keySpawnIndex];
        m_syringeSpawnPoint = m_spawnPoints[syringeSpawnIndex];
        m_mirrorSpawnPoint = m_spawnPoints[mirrorSpawnIndex];

        // Spawn the items in the generated spawn points, and set those spawn points as their parents
        Instantiate(m_key, m_keySpawnPoint.transform.position, Quaternion.identity, m_keySpawnPoint.transform);
        Instantiate(m_syringe, m_syringeSpawnPoint.transform.position, Quaternion.identity, m_syringeSpawnPoint.transform);
        Instantiate(m_mirror, m_mirrorSpawnPoint.transform.position, Quaternion.identity, m_mirrorSpawnPoint.transform);
    }
}
