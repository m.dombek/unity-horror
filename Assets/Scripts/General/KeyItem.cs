﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyItem : MonoBehaviour
{
    private bool m_canPickUp = false;
    private Player m_player;
    private string m_itemName;
    private UIManager m_uiManager;
    private AudioSource m_pickUpSound;

    // Start is called before the first frame update
    void Start()
    {
        m_player = GameObject.Find("Player").GetComponent<Player>();
        m_uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        m_pickUpSound = GameObject.Find("PickUpSound").GetComponent<AudioSource>();
        transform.localScale *= 3;
    }

    private void Update()
    {
        if (m_canPickUp && Input.GetKeyDown(KeyCode.E))
        {
            m_player.IncreaseInventory();
            m_canPickUp = false;
            m_uiManager.SetPickupText(false);
            m_pickUpSound.Play();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (transform.childCount > 0)
        {
            m_uiManager.SetPickupText(true);
            m_canPickUp = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (transform.childCount > 0)
        {
            m_uiManager.SetPickupText(false);
            m_canPickUp = false;
        }
    }
}
