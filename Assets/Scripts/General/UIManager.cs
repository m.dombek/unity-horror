﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Text m_pickUpText;

    [SerializeField]
    private Text m_deadText;

    [SerializeField]
    private Text m_restartText;

    [SerializeField]
    private RawImage m_staticImage;

    private bool m_enableStaticEffect = false;
    private float m_alphaDeltaValue = 0.01f;
    private float m_healRate = 0.01f;
    private GameManager m_gameManager;

    private void Start()
    {
        m_gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        float currentStaticAlphaValue = m_staticImage.color.a;        

        if (m_enableStaticEffect)
        {
            currentStaticAlphaValue = Mathf.Min(currentStaticAlphaValue + m_alphaDeltaValue, 255);
        }
        else
        {
            currentStaticAlphaValue = Mathf.Max(currentStaticAlphaValue - m_healRate, 0);
        }

        m_staticImage.color = new Color(m_staticImage.color.r, m_staticImage.color.b, m_staticImage.color.g, currentStaticAlphaValue);

        if (currentStaticAlphaValue > 2.5f)
        {
            m_gameManager.GameOver();
        }
    }

    public void SetPickupText(bool value)
    {
        m_pickUpText.gameObject.SetActive(value);
    }

    public void SetStaticImageVisible(bool visible, bool enemySeenByPlayer)
    {
        // If the Player sees the enemy
        if (enemySeenByPlayer)
        {
            m_enableStaticEffect = visible;
        }
        else
        {
            // In case the Player is seeing the enemy but the enemy is too far away, prevent static effect from fading
            if (!m_enableStaticEffect)
            {
                m_enableStaticEffect = visible;
            }
        }
    }

    public void SetDeathText(bool value)
    {
        m_deadText.gameObject.SetActive(value);
        m_restartText.gameObject.SetActive(value);
    }
}
